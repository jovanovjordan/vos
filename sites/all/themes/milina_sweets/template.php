<?php

drupal_add_js(path_to_theme().'/js/vebko.js');
drupal_add_js(path_to_theme().'/js/jquery.snow.min.1.0.js');


function milina_sweets_preprocess_html(&$variables) {
	drupal_add_css('http://fonts.googleapis.com/css?family=Dosis:300,500,700', array('type' => 'external'));
	drupal_add_css('http://fonts.googleapis.com/css?family=Roboto+Condensed', array('type' => 'external'));
	drupal_add_css('http://fonts.googleapis.com/css?family=Ubuntu+Condensed', array('type' => 'external'));
	drupal_add_css('http://fonts.googleapis.com/css?family=Oxygen:400,300', array('type' => 'external'));
	drupal_add_css('https://fonts.googleapis.com/css?family=Lobster|Open+Sans+Condensed:300', array('type' => 'external'));
}

function milina_sweets_form_alter(&$form, &$form_state, $form_id) {
    if ($form_id == 'contact_site_form') {
        $form['#submit'][] = 'contact_form_submit_handler';
    }
}

function contact_form_submit_handler(&$form, &$form_state) {
$path = current_path();
    $form_state['redirect'] = $path;
}

function manualMetaTags($path) {
$path = current_path();
$path_alias = drupal_lookup_path('alias',$path);
$path = drupal_get_path_alias($path);

if(drupal_is_front_page()||($path=='tv')||($path=='services')||($path=='products')||($path=='solutions')){
  // determine the path of the page
  
	global $language ;
	$lang_name = $language->language ;
  $path = 'facebook'.$path.$lang_name; 
  //echo $path; exit;
  switch ($path) {     
    case "facebooktvmk":
    $url= "http://vos.mk/tv'>";
     $title = "Тв Емисии 'Венчавка од соништата'";
        $description = "Тв Емисии - Венчавка од соништата.";
        $keywords    = "Венчавка од соништата, свадба, венчавнка";
        $image = "vlogofb.jpg";
        break;
       
    default:
       // if all else fails...
       $url= "http://vos.mk'>";
       $title = "Венчавка од соништата";
       $description = "Тв Емисии - Венчавка од соништата.";
       $keywords    = "Венчавка од соништата, свадба, венчавнка";
       $image = "vlogofb.jpg";
  }
 
  // output
 
  
  print "<meta name='description' content='".$description."' />\n";
  print "<meta name='keywords' content='".$keywords."' />\n";
  print "<meta name='fb:app_id' content='EzhoKqt1gFD' />\n";
  print "<meta property='og:title' content='".$title."'>\n";
  print "<meta property='og:description' content='".$description."'>\n";
  print "<meta property='og:locale' content='en_US'>\n";
  print "<meta property='og:url' content='".$url."\n";
  print "<meta property='og:site_name' content='Vebko - Web Solutions'>\n";
  print "<meta property='og:image' content='http://vos.mk/sites/all/themes/milina_sweets/images/".$image."'>\n";
  print "<meta property='og:image:type' content='image/jpeg'>\n";
  
  
  }


}

function milina_sweets_preprocess_node(&$vars) {
	
	if (!drupal_is_front_page()){						
        if ($vars['type'] == 'media') {
			$og_title = $vars['title'];
			$og_title_single = $og_title;
			$og_title = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:title', 'content' => $og_title_single , ), ); 
			drupal_add_html_head($og_title , 'og_title');

			
			$og_descriptions = $vars['body']; 
			$og_descriptions_single = ($og_descriptions ['und'][0]['value']);
			$og_descriptions = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:description', 'content' => $og_descriptions_single , ), ); 
			drupal_add_html_head($og_descriptions , 'og_descriptions');

			$img = field_get_items('node', $vars['node'], 'field_banner'); 
			$img_url = file_create_url($img[0]['uri']);
			$og_image = array( '#tag' => 'meta', '#attributes' => array( 'property' => 'og:image', 'content' => $img_url, ), ); 
			drupal_add_html_head($og_image, 'og_image');
		}
	}
}